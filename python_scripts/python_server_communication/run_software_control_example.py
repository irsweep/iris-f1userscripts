# -*- coding: utf-8 -*-
"""
Copyright (c) 2018 - present, IRsweep AG
MIT license
"""

import sys,os


from python_server_communication.python_client import python_client
import time


#%% Connect to hardware (Type ConnectNew in IRis-F1 GUI first)
control = python_client()
control.connect_to_hardware(serverIP = "10.131.101.1") # Replace IPs by those of your devices

#%% Switch on the lasers
# control.sendCommand('LaserStart')

#%% Find good laser conditions and enter them here
# I0 and I1 are the currents of laser 0 and laser 1 in ampare. T0 and T1 are the temperatures of laser 0 and 1 in °C
control.start()
control.set_conditions(I0 = 0.465,
                      I1 = 0.4645,
                      T0 = -1.5,
                      T1 = -7)


#%% Set measurement parameters (see readme file for a list of settings or python_client class for a description)
control.set_measurement_settings(
                                filename='test',
                                DataProcessing = 1,
                                ParallelProcessing = 1,
                                NumberOfAveragesBackground = 10,
                                NumberOfAveragesSample = 1,
                                NumberOfAveragesTransfer = 10,
                                NumberOfMeasurementsSample = 0,
                                PretriggerTime = 1,
                                PretriggerAcquisitions = 0,
                                Samples = 2**25,
                                FftLength = 2**15,
                                SaveRaw = 0,
                                SaveTransmission = 1,
                                Processor = 'LongTerm',
                                TriggerLevel = 0.5,
                                AcquisitionFrequency = 0,
                                QueueLength = 0,
                                UseBackgroundIntegrationTime = 0,
                                BackgroundIntegrationTime = 0
                                )

#%% set the trigger mode; options are: Internal, SingleExternal, MultiExternal
control.set_trigger('Internal')

#%% start acquiring data
control.start()

#%% measure a tranfer function
control.measure_transfer()

#%% Take a background measurement
control.measure_background()

#%% recods a measurement. Make sure to record first a background
control.measure_sample(filename='test_run_python_client')

#%% stop acquiring data
control.stop()

#%% Switch off the lasers
control.sendCommand('LaserStop')

