# -*- coding: utf-8 -*-
"""
Copyright (c) 2018 - present, IRsweep AG
MIT license
"""

import telnetlib
import time

class RelayBoard():
    '''
    This class can be used to switch a relay board connected via Ethernet to 
    the instrumnet network. It uses the telnet communication acting as a client
    '''
    def __init__(self,board_address):
        self.board_address=board_address
        self.commandSleepTime = 1
        
        
    def connectRelay(self):
        '''
        Establishes a connection to the relay board and initializes the
        default state

        Returns
        -------
        None.

        '''
       # DeviceIP = "10.131.101.30"  
        tn = telnetlib.Telnet(self.board_address,23)
        log_result = tn.read_until(b"\nconnected\r\n",timeout=1)
        self.tn=tn
        for i in range(1,3):
            self.switchRelay(i,False)
        
        
    def closeConnection(self):
        '''
        This method closes the connection to the relay board server and switches
        all relays to off

        Returns
        -------
        None.

        '''
        for i in range(1,3):
            self.switchRelay(i,False)
        self.tn.close()
         
     
    def sendCommand(self,command):
        '''
        Method that transforms the str(command) to a binery and sends the command
        to the established connection. If the connection is lost it returnes an
        error.

        Parameters
        ----------
        command : str
            command to be send.

        Raises
        ------
        ValueError
            Error if the connection is lost.

        Returns
        -------
        bool
            Check if the command was set by the relay bord.

        '''
        if not hasattr(self,'tn'):
            raise ValueError("connection lost")
        
        self.tn.write(command.encode("ascii") + b"\r\n")
        time.sleep(self.commandSleepTime)
        response = self.tn.read_eager()
        if (b"\n"+command.encode("ascii")+b"\r\n")==response:
            return True
        else:
            return False

    def switchRelay(self, channel,state):
        '''
        Method to switch the defined relays on the board. It accepts the channels 
        1 and 2 for relay 1 and 2.

        Parameters
        ----------
        channel : int
            1 is relay 1 and 2 is relay 2.
        state : boolen
            True switches the channel on and Fals off.

        Returns
        -------
        boolen
            Returns a check of the channel was switched.

        '''
        if channel==1 and state==True:
           return self.sendCommand('R1On')
        
        if channel==1 and state==False:
            return self.sendCommand('R1Off')
        
        if channel==2 and state==True:
            return self.sendCommand('R2On')
        
        if channel==2 and state==False:
            return self.sendCommand('R2Off')
        
    def sendTrigger(self):  
        '''
        Method to send a command generating a 50 us trigger with a value of 5V

        Returns
        -------
        boolen
            Returns a check of the trigger was send.

        '''
        return self.sendCommand('Trig')

             
if __name__=='__main__':
    
    DeviceIP = "10.131.101.30"
    relay = RelayBoard(DeviceIP)
    relay.connectRelay()

    
        