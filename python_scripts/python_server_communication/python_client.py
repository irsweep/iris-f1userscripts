# -*- coding: utf-8 -*-
"""
Copyright (c) 2018 - present, IRsweep AG
MIT license
"""

import os,sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

# from common_libraries.heterodyne_postprocessing.processing.postProcessor import PostProcessor 

#import paramiko
#from scp import SCPClient
import h5py
import numpy as np
import time
import json
import warnings
#import pytest

from python_server_communication.PyToServer import PyToServer
from python_server_communication.relayBoard import RelayBoard


class python_client():
    def __init__(self):           
        super().__init__()
        
        self.local_path = '/home/spectroscopist/Documents/data/tests'
        self.triggerSetting = 'Internal'
        self.measurement_settings = {
                     'SaveFilename'                 : 'testing',   # String, filename of measurement file
                     'AcquisitionFrequency'         : 10.0,        # Float, highest frequency of acquisitions
                     'Processor'                    : 'LongTerm',  # String, toggels between 'LongTerm' and 'TimeResolved' data processing
                     'DataProcessing'               : 1,           # Integer, chooses if data is being processed (1) or not (0)
                     'SaveTransmission'             : 1,           # Integer, chooses if transmission data is being saved (1) or not (0)
                     'SaveRaw'                      : 0,           # Integer, chooses if raw data is being saved (1) or not (0)
                     'TriggerLevel'                 : 0.9,         # Float, threshold value for trigger in V
                     'FftLength'                    : 2**15,       # Integer, nummber of samples per FFT
                     'NumberOfAveragesBackground'   : 1,           # Integer, number of acquisitions averaged for background measurement
                     'NumberOfAveragesSample'       : 1,           # Integer, number of acquisitions averaged per sample measurement
                     'NumberOfAveragesTransfer'     : 10,          # Integer, number of acquisitions averaged for transfer function measurement
                     'NumberOfMeasurementsSample'   : 0,           # Integer, number of sample measurements (with each NumberOfAveragesSample acquisitions) before measurement stop. 0 means infinite
                     'PretriggerTime'               : 1.0,         # Float, time of pre-trigger acquisition in ms
                     'PretriggerAcquisitions'       : 0,           # Integer number of acquisitions taken prior to trigger in single trigger mode
                     'Samples'                      : 2**25,       # Integer, number of samples per acquisition
                     'ParallelProcessing'           : 1,           # Integer, chooses if data is being processed while acquired (1) or not (0)
                     'QueueLength'                  : 0,           # Specify the maximum buffer size
                     'UseBackgroundIntegrationTime' : 0,           # Allows the background integration time to be selected independently from the pretrigger time. (1) or not (0)
                     'BackgroundIntegrationTime'    : 0,           # The time (in ms) used for the background calculation. Only active when UseBackgroundIntegrationTime is 1
                     }
        
    
    def connect_to_hardware(self, serverIP = "10.131.101.1", deviceIP = "10.131.101.30", PrintFeedback = True):
        """
        Establish a connection with the IRsF1 and an external triggering if available

        Parameters
        ----------
        serverIP : str, optional
            IP adress to the server. The default is "10.131.101.1".
        deviceIP : str, optional
            IP adress on the relayboard/triggering hardware. The default is "10.131.101.30".
        PrintFeedback : boolean, optional
            Option to print out the messages received from the IRis-F1. The default is True.

        Returns
        -------
        None.

        """

        self.connection = PyToServer(serverIP)
        time.sleep(1)
        self.connection.delay = 0.001

        if PrintFeedback == True:
            self.connection.initReadingBoost()
            self.connection.startThreads()
            self.connection.enablePrintFeedback()
            

        try:  
            self.relay = RelayBoard(deviceIP)
            self.relay.connectRelay()
            self.relay.commandSleepTime = 0
            print('Connected to '+str(deviceIP))
            time.sleep(1)
        except:
            warnings.warn('External triggering device could not be connected')
            pass


        
    
    def set_measurement_settings(self,filename, **kwargs):
        """Method to set measurement parameters. Refer to the user manual for more detail.

        Parameters
        ----------
            filename (str): Filename prefix of saved measurement. 
            **kwargs: any keyword argument. The measurement_settings are only updated, if the keyword was initialized in __init__().
        
            Returns
            -------
            None.
        """
        
        assert isinstance(filename, str), "Please provide a filename"
        self.connection.sendCommand("Stop")
        self.connection.delay = 0.001
        time.sleep(1)
        
        self.measurement_settings['SaveFilename'] = filename
        for measSetting in kwargs:
            if measSetting in self.measurement_settings:
                self.measurement_settings[measSetting] = kwargs[measSetting]
            else:
                raise KeyError('Unknown setting: ' + measSetting)
        
        for setting in self.measurement_settings:
            self.connection.sendCommand(setting+":"+str(self.measurement_settings[setting]))
            
        time.sleep(1)

        print("Measurement conditions set")

    
    def set_conditions(self, I0 = None, I1 = None, T0 = None, T1 = None):
        """
        Change laser currents and temperatures according to input. 

        Parameters
        ----------
        I0 : float
            Current of laser 1. 
        I1 : float
            Current of laser 2. 
        T0 : float
            Temperature of laser 1. 
        T1 : float
            Temperature of laser 2.

        Returns
        -------
        None.

        """
        print('Setting laser parameters.')
        if I0 is not None:
            self.connection.sendCommand("Current:0:"+str(I0))
            self.I0 = I0

        if I1 is not None:
            self.connection.sendCommand("Current:1:"+str(I1))
            self.I1 = I1

        if T0 is not None:
            self.connection.sendCommand("Temperature:0:"+str(T0))
            self.T0 = T0

        if T1 is not None:
            self.connection.sendCommand("Temperature:1:"+str(T1))
            self.T1 = T1

    
    def write_params_to_file(self, filename = None, local_path = '/home/spectroscopist/Documents/data/tests'):
        """
        Writes the measurement parameters to a text file.

        Parameters
        ----------
        filename : str
            name of the measurement
        
        local_path : str
            location of the resulting file

        Returns
        -------
        None.

        """
        if filename == None:
            filename = self.measurement_settings['SaveFilename']
            
        self.local_path = local_path
        filename_w_path = self.local_path +'/'+ filename + '_parameters.txt'
        
        with open(filename_w_path, 'w') as file:
              file.write(json.dumps(self.measurement_settings)) 
    

    def measure_transfer_automatic(self, I0_off, I1_off, NumberOfAveragesTransfer):
        """
        Runs the transfer measurement, turns off the laser current before executing.
        Make sure the _off currents are below the threshold and above the minimum, otherwise the instrument might crash!
        Refer to LIV graph in laser specification sheet to find out where the laser is off.

        Parameters
        -------
            I0_off : float
            current at which laser 1 is off
            
            I1_off : float
            current at which laser 2 is off
            
            NumberOfAveragesTransfer : int
            number of transfer functions to take
        Returns
        -------
        None.

        """
        self.connection.sendCommand("NumberOfAveragesTransfer:" + str(NumberOfAveragesTransfer))
        self.NumberOfAveragesTransfer = NumberOfAveragesTransfer
        
        I0_good = self.I0
        I1_good = self.I1
        
        self.set_conditions(I0_off, I1_off, T0 = self.T0, T1 = self.T1)
        time.sleep(1)
        self.measure_transfer()
        
        self.set_conditions(I0_good, I1_good, T0 = self.T0, T1 = self.T1)
        print("Please wait for the temperature to re-stabilize. This can take a minute.")
        time.sleep(5)
        

    def measure_background(self):
        """
        Runs the background measurement.
        
        Returns
        -------
        None.

        """
        self.set_trigger(trigger = 'Internal', overwrite = False)
        self.startAcquisitionCard()
        self.connection.sendCommand("MeasureBackground")
        self.waitToFinish()            
        self.set_trigger()


    def measure_transfer(self):
        """
        Runs the transfer measurement, make sure to close the shutters before executing.
        If this is not possible, consider using measure_transfer_automatic().

        Returns
        -------
        None.

        """
        self.set_trigger(trigger = 'Internal', overwrite = False)
        self.startAcquisitionCard()
        self.connection.sendCommand("MeasureTransfer")
        self.waitToFinish()
        self.set_trigger()


    def set_trigger(self, trigger=None, overwrite = True):
        """Set trigger according to input, otherwise fall back to triggerSetting.
        
        Parameters
        -------
            trigger : string
            The desired trigger setting. Checked against hardcoded approved settings.
            
            overwrite : Boolean
            Determines if the global trigger setting is to be updated

        Returns
        -------
        None.
        
        """
        approved_settings = ['Internal', 'MultiExternal', 'SingleExternal', 'H5']
        
        self.connection.sendCommand("Stop") 
        
        if trigger == None:
            trigger = self.triggerSetting
        elif trigger not in approved_settings:
            raise TypeError(str(trigger)+' is not a valid trigger setting.')

        self.connection.sendCommand("Driver:"+trigger)
        self.connection.sendCommand("ChangeDriver")
        
        if overwrite:
            self.triggerSetting = trigger
        
        
    def waitToFinish(self):
        """Sleeps until the server confirms the acquisition has finished. 
        
        Returns
        -------
        None.
        """
        while(not self.connection.seenFinishedSample):
            print('recording')
            time.sleep(1) 
        self.connection.seenFinishedSample=False
        self.connection.sendCommand('Stop')
        time.sleep(1)


    def start(self):
        """This method sets the trigger to internal and starts the acquisition card. 
        The original trigger must be restored by other functions once the measurement is started.
        """
        if self.triggerSetting != 'Internal':
            self.set_trigger(trigger = 'Internal', overwrite = False)
        self.connection.sendCommand('Start')
        time.sleep(2)


    def stop(self):
        """Stops the acquisition and returns the trigger to the saved value.
        
        Returns
        -------
        None.
        """
        self.connection.sendCommand('Stop')
        self.set_trigger()
        

    def startAcquisitionCard(self):
        """This method starts the acquisition card with the command "Start". But as this process takes time a delay is added
        """
        self.connection.sendCommand('Start')
        time.sleep(2)


    def measure_sample(self, filename=None):
        """records a measurement with the current settings.

        Args:
            filename ([str], optional): Filename for saving. Defaults to None.
        """
        
        self.set_trigger()

        if filename is not None:
            self.measurement_settings['SaveFilename'] = filename
            self.connection.sendCommand("SaveFilename:"+filename)
        
        self.startAcquisitionCard()
        self.connection.sendCommand("MeasureSample")
        
        if self.measurement_settings['NumberOfMeasurementsSample']!=0:    
            self.waitToFinish()


    def useH5file(self,rawFilename=None,rawDirectory=None):
        """This method is to switch the driver to work with raw files intead of live data from the acquisition card

        Args:
            rawFilename ([str], optional): sets the file name of the raw file (without .h5). Defaults to None.
            rawDirectory ([str], optional): sets the directory of the raw file (use a / at the end). Defaults to None.
        """
        self.connection.sendCommand("Stop") 
        self.connection.sendCommand("Driver:H5")
        self.connection.sendCommand("ChangeDriver")
        
        if rawFilename is not None:
            self.connection.sendCommand("RawDataFilenameH5:"+rawFilename)
        
        if rawDirectory is not None:
            self.connection.sendCommand("RawDaqDataWriteToDiskDirectory:"+rawDirectory)
            
            
    def sendCommand(self, command):
        """
        Send commands to the IRis-F1, as if using the command line on the IRis-GUI.

        Parameters
        ----------
        command : str
            Any command accepted by the current IRis-F1 server (see documentation for more info).

        Returns
        -------
        None.

        """
        self.connection.sendCommand(command)       
        
    def useLiveData(self):
        """Method to set the driver to use the acquisition card to record data 
        """
        self.set_trigger(trigger='Internal')
        
        