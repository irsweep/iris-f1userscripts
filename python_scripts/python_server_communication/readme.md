############################################################
Welcome to the python_server_communication readme file.
Here we describe how to control the IRis-F1 or IRis-core through a python script.


##### Hardware and software requirements #####
Control through python can be set up with any IRis-F1 or IRis-core, running on software v6 (or later). You need to have a running installation of python on your control computer. We recommend using Spyder installed via Anaconda. 
Optionally, you can also use an external relay board to trigger the spectrometer. Both the relay board and the spectrometer can be controlled through the same set of scripts.


##### Script structure #####
The relevant scripts are stored in the folder python_server_communication and are designed to interact as depicted below. There are three levels:

user_defined_script.py        user_defined_script_2.py        run_software_control_example.py
		 |                             |                                    |
		 |                             |                                    |
 		 |                             |                                    |
		 |_____________________________|____________________________________|
		 |
		 |
		 |
	python_client.py
		 |
		 |
 		 |__________________________________
						|					|
		           PyToServer.py      RelayBoard.py


Level 1:
At the bottom level, PyToServer.py and RelayBoard.py control how the python interacts with the spectrometer and an (optional) relay board. We do not recommend changing anything in these files, as it can cause problems in connecting to and communicating with your spectrometer. 

Level 2:
The file python_client.py the functions from PyToServer.py and RelayBoard.py. It contains relatively high-level functions, which can be used to take direct control of the spectrometer. These functions have been written with safe operation of the spectrometer in mind. For an up-to-date list of available functions, take a look at the contents of python_client.py. 

Of particular note is the function sendCommand(), which allows the user to send a command directly to the spectrometer, in a similar way to using the command line in the GUI. Using sendCommand(), one can build functions corresponding to a set of parameters needed for an experiment. Please note that using this command gives far more oppertunities for crashing the instrument compared to manual operation, since there are no safeguards against sending commands in incorrect sequences or with insufficient delays between them. We therefore recommend using the pre-written functions in python_client.py whenever possible. 

Level 3:
On the third level are user-defined scripts, which inherit all the functions from python_client.py. These files are the main point of interaction and provide a convenient way to store experimental parameters, protocols and settings. An example is provided in the file run_software_control_example.py. Users can write their experiments into copies of this file. 


##### Instructions for use #####	
Open the script run_software_control_example.py in Spyder and execute the first cell. 
Start the spectrometer normally. After you click "Connect" on the GUI, enter the following command in the Command Line interface of the GUI: ConnectNew
You can now execute the second cell, containing the command connect_to_hardware(). The spectrometer can now be controlled by sending commands through either the python console or by running cells with defined command-sequences. You can still use the GUI as normally and also use it to view data being acquired. 


##### Spectrometer control functions #####
The following functions are designed to control the spectrometer in a safe way, analogous to using the GUI directly. In the default script, use as control.function()

start()								Starts the acquisition (even when the trigger is not set to 'Internal')
stop()								Stops the acquisition
set_trigger('setting')				Changes the trigger setting; accepts 'Internal', 'SingleExternal', 'MultiExternal', and 'H5'
measure_transfer()					Measures the transfer function with 'Internal' triggering mode; restores the user's trigger setting at the end of the measurment  
measure_transfer_automatic()		Same as measure_transfer() but the lasers are turned off automatically by reducing the current; returns to the laser conditions defined in set_conditions() at the end of the measurement 
measure_background() 				Takes a background measurement in long-term mode with 'Internal' triggering mode; restores the user's trigger setting at the end of the measurment  
measure_sample('filename')			Takes a sample measurement; the filename is required; always respects the user's trigger setting
set_measurement_settings(
	'filename',kwargs)				Changes the spectrometer settings; the filename is required, all other arguments are optional; see script for possible settings
set_conditions(I0, I1, T0, T1)		Sets laser conditions; the values are provided as floats


##### List of commands #####
What follows is a list of commands that can be used with the function sendCommand(). Note that all commands are sent through python as strings, e.g.: sendCommand('NumberOfMeasurementsSample:5')

**** General commands ****
Start (str)							Starts acquisition
Stop (str)							Stops acquisition
MeasureX (str)						Starts chosen measurement, e.g.: 'MeasureTransfer', 'MeasureBackground', 'MeasureSample'
Processor (str)						String, toggels between 'LongTerm' and 'TimeResolved' data processing
DataProcessing (bool)				Data processing can be turned off to increase write speed of raw data to disk. Default: 1
Current:[laser]:[value]				Sets the current on the 'laser'(0 - Laser1, 1 - Laser2) to the 'value'. Respects module limits
Temperature:[laser]:[value]			Sets the temperature on the 'laser'(0 - Laser1, 1 - Laser2) to the 'value'. Respects module limits

**** Measurement setup commands ****
Samples	(int)						Size of the acquisitions measured as number of samples. Must a power of 2. To convert to acquisition length (ms), divide by 2x10^6. Default: 33554432
NumberOfAveragesTransfer (int)		Number of acquisitions used for the transfer function. Default: 10
NumberOfAveragesBackground (int)	Number of acquisitions used for the background. Default: 10
NumberOfMeasurementsSample (int)	Number of acquisitions used for the sample measurement. A value of 0 means the measurement continues until it is stopped manually. Default:0
NumberOfAveragesSample (int)		Number of acquisitions averaged during a sample measurement. Used to avoid large file sizes. Default: 1
AppendDateH5 (bool)					Append the timestamp to the HDF5 file. Warning: files will be overwritten if off. Default: 1
SaveDirectory (str)					HDF5 file save directory. Default: '/data/HDF5Data'
SaveFilename (str)					HDF5 saved filename
SaveRaw	(bool)						Save raw data to the HDF5 file if true. Default: 0
SaveTransmission (bool)				Save processed transmission data to the HDF5 file. Default: 1

**** Triggering commands ****
AcquisitionFrequency (float)		Set a desired acquisition frequency (in Hz) of the multi-heterodyne signal acquisition. A value of 0 means the measurement proceeds as fast as possible (parameter-dependent). Default: 10.0 Hz
ParallelProcessing (bool)			Enables data processing while recording. May decrease acquisition frequency. Default: 1
QueueLength	(int)					Specify the maximum buffer size
PretriggerTime (float)				Time-resolved measurement only. Specify how long before the trigger (in ms) the measurement starts. This time will also be used to calculate the background for each acquisition. Default: 0 
TriggerDelay (float)				Manually shift the acquisition start pre- or post trigger (in ms). Does not affect background calculation. Default: 0 ms
UseBackgroundIntegrationTime (bool)	Allows the background integration time to be selected independently from the pretrigger time. Default: 0.
BackgroundIntegrationTime (float)	The time (in ms) used for the background calculation. Only active when UseBackgroundIntegrationTime is 1. The value can be set between 0 and the length of the measurement. Default: 0
PretriggerAcquisitions (int)		Take a number of acquisitions before waiting for external trigger. This setting is only effective with MeasureOnSingleTrigger on. Default: 0.
TriggerLevel (float)				Voltage to trigger from external input from trigger channel. Default: 0.5

**** Advanced processing commands ****
FftLength (int)						FFT length. Must be 2^N
Interleaving (int)					Interleaving factor. Enhances the signal processing SNR by interleaving. Given in power of 2 (Processing requirement is roughly multiplied by 2powN). Default: 1
ZeroPadding	(int)					Zero padding data for signal processing. Enhances the signal processing SNR by zero padding. Given in power of 2 (Processing requirement multiplied by 2powN). Default: 1

############################################################