# -*- coding: utf-8 -*-
"""
Copyright (c) 2018 - present, IRsweep AG
MIT license
"""

import socket
import threading
import time

class PyToServer():
    ''' Class that enables the user to establish a connection to the server and pass commands to it.'''

    def __init__(self, server_address):
        self.server_address=server_address
        self.connectToSocket()

        # self.delay=0.5   # delay time in seconds between commmand and retrieving server message, this time can be increased to ensure correct server feedback message. TODO: when multicast is ready, the delay can be taken off
        
        self.bit=''
        self.bit2=''
        self.reading = True
        self.printFeedback = True
        self.seenFinishedSample=False # consider renaming to seenFinishedAcquisition since it now also includes TF and BG
        self.threads=list()


    def threadConstructor(self,thread):
        """Method to add diferent threads to the thead list

        Args:
            thread ([method]): initializes individual threads and add for parallel running
        """
        x = threading.Thread(target=thread, args=())
        self.threads.append(x)

        

    def startThreads(self):
        """Starts the defined threads in the self.threads list if they are not yet running
        """
        self._is_running=False
        time.sleep(0.5)
        self._is_running=True
        for i,thread in enumerate(self.threads):
            if not thread.is_alive():
                try:
                    thread.start()
                except:
                    pass


    def initReadingBoost(self):
        """Initialization method for the thread readFromServer
        """
        self.reading=True
        self.delay=0.5  # wait 0.5 seconds in between commands to ensure correct reading from the server
        self.threadConstructor(self.readFromServer)


    def accept(self, visitor):
        visitor.visit(self)

    def connectToSocket(self):
        '''
        Starts connection to socket. Prints if the connection was established or not.
        '''
        self.sock1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        server_address1 = (self.server_address, 2015)
        server_address2 = (self.server_address, 2016)

        print('Connecting to %s port %s' % server_address1)
        print('Connecting to %s port %s' % server_address2)

        try:
            self.sock1.connect(server_address1)
            self.sock2.connect(server_address2)
        except:
            print("Couldn't complete connection.")
            raise
        else:
            print("Connection established.")

    
    def closeConnection(self):
        '''
        Closes the sockets connections and stops the thread.
        '''
        self.reading=False
        # self.thread.join() #TODO FIX
        self.sock1.close()
        self.sock2.close()
   
    def make_header(self, command):
        '''
        Makes the header. The header is required by the C++ code in the server.

        For example: Limits becomes 000000000006Limits
        '''
        return '0'*(12-len(str(len(command))))+str(len(command))+command

    def strip_header(self, receivedMessage):
        '''
        Handles the 12 digits on the header and splits the message into lines.

        For example: 000000000017SET:Interlock:OFF becomes SET:Interlock:OFF
        '''
        numChar_str=''
        finalMessage=''
        counter=1
        header=True
        for x in receivedMessage:
            if header:
                if counter<12: # if on header
                    if x!='0':
                        numChar_str+=x
                elif counter==12:
                    numChar_str+=x
                    numChar=int(numChar_str)
                    numChar_str=''
                    counter=0
                    header=False
            elif counter<numChar:
                finalMessage+=x
            elif counter==numChar:
                finalMessage+=x+'\n'
                counter=0
                header=True
            counter+=1
        
        return finalMessage.replace('INFO:', '')

    def formatMessage(self, message, sent_command):
        '''Formats the message for a more comprehensive output. Returns string with final message formatted.'''
        finalMessage=time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())+'\n'
        finalMessage+='Sent Command:\t'+str(sent_command)+'\n'
        finalMessage+="Received Message:\n---------------------------------------------------------------------\n\n"
        # The command 'Help' comes with a different formatting that gives an error when decoding the bytes and often comes chopped.
        # So a selection is made and an identical 'Help' answer is output here
        if sent_command!='Help':
            return finalMessage+self.strip_header(message)
        else:
            return "\n\n!!! This message was produced by this code. The \"Help\" message was handled to be displayed in the correct format. !!!\r\n"+ finalMessage+"Commands\r\n" +"--------------------\r\n" +"Current:[laser]:[value]\tSets the current on the 'laser'(0 - Laser1, 1 - Laser2) to the 'value'. Respects module limits.\r\n" +"Digital:[channel]:[0/1]\tSets the digital state on the 'channel' to the 'value'.\r\n" +"LaserStart\tStarts laser start procedure.\r\n" +"LaserStop\tStarts laser stop procedure.\r\n" +"Limits\tPrints actually used modules limits.\r\n" +"MeasureX\tStarts chosen measurement, ex: 'MeasureTransfer', 'MeasureSample'.\r\n" +"Start\tStarts acquisition.\r\n" +"Stop\tStops acquisition.\r\n" +"Temperature:[laser]:[value]\tSets the temperature on the 'laser'(0 - Laser1, 1 - Laser2) to the 'value'. Respects module limits.\r\n" +"Voltage:[channel]:[value]\tSets the voltage on the 'channel' to the 'value'. Respects module limits.\r\n" +"----------------------------------------------------------------------\r\n" +"DAQ options\r\n" +"--------------------\r\n" +"AcquisitionFrequency\tSet a desired acquisition frequency of the multi-heterodyne signal acquisition. Default: 10.0 Hz. Frequency: 0 Hz: as fast as possible\r\n" +"BackgroundIntegrationTime\tThe time used for the background calculation. This value is only used when “Independent trigger and background” is true. Value can be set between 0 and the length of the measurement (in ms).\r\n" +"BandwidthFilter\tTurn on high-pass filter on DAC board. Cut-off 650 MHz. Default: On\r\n" +"Channels\tNumber of desired acquisition channels\r\n" +"ConvertVolt\tConvert data acquisition values to volt. Overhead in processing. Default: No\r\n" +"ExtRef\tUse 100 MHz external reference clock\r\n" +"MeasureOnSingleTrigger\tDecide if card should wait for a first external trigger, all subsequent acquisitions after will be software triggered\r\n" +"MeasureOnTrigger\tSetting to choose to acquire a measurement on a trigger. Default: Off\r\n" +"Offset\tInput signal offset. Default 0.0V\r\n" +"ParallelProcessing\tDecide if processor should start processing when buffer is filled, or process along the data acquisition\r\n" +"PretriggerAcquisitions\tNumber pretrigger measurement taken on software trigger before waiting for external trigger. This setting is only effective with MeasureOnSingleTrigger on.\r\n" +"PretriggerTime\tIn ms. Keep measurement data for a set amount of time before the trigger arrives. The total acquisition time is unaffected by this value. The maximum value of PreTriggerTime is equal to the measurement time.\r\n" +"Range\tInput signal range in Volts. Default 1.0V\r\n" +"SampleRate\tThis setting is ignored for Teledyne card and it works by default at 2GS/s rate. The SampleRate setting is used to calculate the effecite sample rate. The effective sample rate depends also on the SampleRateDivider and the number of Channels: EffectiveSampleRate = SampleRate / (Channels * SampleRateDivider).The EffectiveSampleRate constraints are: Max: 1.6 GS/s for two channels. 3.2 GS/s for one channel. Min: 1.5625 MS/s for two channels. 3.125 MS/s for one channel. SampleRate default value: 3.2e9. \r\n" +"SampleRateDivider\tUnused for Teledyne card. Integer indicating by how much the SampleRate should be divided\r\n" +"Samples\tSize of data samples acquired with DAC. Must be multiple of fft-length and power of 2\r\n" +"Simulate\tSimulate acquistion driver (Agilent). Default: No\r\n" +"TriggerDelay\tAcquisition starts pre- or post trigger by number of ms. Default: 0 ms\r\n" +"TriggerLevel\tMinimal voltage to activate external trigger\r\n" +"UseBackgroundIntegrationTime\tAllows the background integration time to be selected independently of the pretrigger time.\r\n" +"----------------------------------------------------------------------\r\n" +"General options\r\n" +"--------------------\r\n" +"BackgroundDetectionThreshold\tDetection threshold for minimum multi-heterodyne peak height in reference beam. Default: 0.0\r\n" +"Bkg\tAcquisition is background. Default: No\r\n" +"DataProcessing\tData processing can be turned off to increase write speed of raw data to disk. Default: on.\r\n" +"FftLength\tFFT length. Must be 2^N\r\n" +"Interleaving\tInterleaving factor. Enhance the signal processing SNR by interleaving. Given in power of 2 (Processing requirement is roughly multiplied by 2powN). Default: 1\r\n" +"NumberOfAveragesBackground\tNumber timeslices of length �Samples� which are averaged when measuring the background. Default: 10\r\n" +"NumberOfAveragesSample\tNumber timeslices of length �Samples� which are averaged when measuring the sample. Default: 1\r\n" +"NumberOfAveragesTransfer\tNumber timeslices of length �Samples� which are averaged when measuring the transfer function. Default 10\r\n" +"NumberOfMeasurementsSample\tNumber of requested Sample measurements. In static mode each measurement is constructed from a number of averaged acquisitions. Default: Infinite (N=0) until stop required\r\n" +"QueueLength\tSpecify the max queue size\r\n" +"DefaultTV\tPlaceholder typed value, in case csv doesn't load\r\n" +"SpectralDomainData\tBool to turn on or off the processing and display of the spectral domain data. Default: off.\r\n" +"ThermistorLocalOscillatorLaser\tType of Laser 2 thermistor\r\n" +"ThermistorSampleLaser\tType of Laser 1 thermistor\r\n" +"Threads\tSpecify the number of threads the program uses\r\n" +"TimeDomainData\tBool to turn on or off sending to client the time domain data. Default: off.\r\n" +"TimeResolvedProcessor\tUse time resolved measurement(true) or static measurement(false)\r\n" +"TotalSampleAcquisitions\tNumberOfMeasurementsSample * NumberOfAveragesSample. Calculated automatically. Default: Infinite (N=0) until Stop required\r\n" +"Window\tSpecify applied window function. Possible: flattop, boxcar. To be included\r\n" +"ZeroPadding\tZero padding data for signal processing. Enhances the signal processing SNR by zero padding. Given in power of 2 (Processing requirement multiplied by 2powN). Default: 1\r\n" +"----------------------------------------------------------------------\r\n" +"Hardware options\r\n" +"--------------------\r\n" +"AqNumber\tActual aq number\r\n" +"Driver\tChoose the used driver, nocard/teledyne/keysight, letter case doesn�t matter\r\n" +"IdleProc\tREAD ONLY, helper value for gui to recognize the current processing state\r\n" +"QclDriverLocalOscillatorLaser\tQclDriver version used for the local oscillator laser\r\n" +"QclDriverSampleLaser\tQclDriver version used for the sample laser\r\n" +"SystemMonitor\tTurn on/off the system monitor data acquisition\r\n" +"TempDir\tDirectory in which the temporary files will be stored (those are deleted after the server is closed)\r\n" +"TemperatureControllerCurrentLocalOscillatorLaser\tCurrent in �A used to drive the thermistor in the temperature controller controlling the local oscillator laser\r\n" +"TemperatureControllerCurrentSampleLaser\tCurrent in �A used to drive the thermistor in the temperature controller controlling the sample laser\r\n" +"----------------------------------------------------------------------\r\n" +"Mode options\r\n" +"--------------------\r\n" +"Interactive\tLaunch program in interactive mode\r\n" +"Server\tLaunch program as server (daemon)\r\n" +"ServerPort\tSpecify the listening port for the server to send commands\r\n" +"ServerPortData\tSpecify the listening port for the server to send data\r\n" +"----------------------------------------------------------------------\r\n" +"Save options\r\n" +"--------------------\r\n" +"AppendDateH5\tAppend current date to the HDF5 file\r\n" +"RawDaqDataWriteToDiskDirectory\tThe directory in which raw acquisition data are saved.\r\n" +"RawDataFilename\tSpecify if raw data from DAQ board should be saved to disk. Argument Default: Acquisition. Default directory: /data/. Only binary version \r\n" +"RawDataFilenameH5\tH5File to read data from\r\n" +"ReadRawData\tWill read saved raw binary data from disk and load it to process the data instead of using the DAQ card. As an argument give the filename including directory and excluding channel and number of acquisition information\r\n" +"SaveDirectory\tHDF5 file save directory\r\n" +"SaveFilename\tHDF5 saved filename\r\n" +"SaveRaw\tWill save raw data to the HDF5 file if true\r\n" +"SaveTimeResolved\tWill save time resolved data to the HDF5 file if true\r\n" +"SaveTransmission\tWill save transmission data to the HDF5 file if true\r\n" +"WriteBufferSize\tWrite buffer size (MB). Default: 0 (write acquisition to disk  immediately)\r\n" +"WriteRawData\tSave raw data to file. Default: Off\r\n"

    def readFromServer(self, bufferSize=9600):
        ''' Reads the server countinuously until PyToServer.reading=False. Outputs any specific output from the server to PyToServer.bit2 '''
        count=0
        while self._is_running:
            while self.reading:
                if count==0:
                    print('Reading from server started')
                    count+=1
                self.bit = self.sock1.recv(bufferSize).decode().strip()
                if (self.bit!="000000000017SET:Interlock:OFF") and (self.bit!="000000000016SET:Interlock:ON"):
                    self.bit2=self.bit
                    if 'FinishedSample' in self.strip_header(self.bit2):
                        self.seenFinishedSample=True
                        print('PY2server message '+self.strip_header(self.bit2))
                    elif 'FinishedTimeResolvedSample' in self.strip_header(self.bit2):
                        self.seenFinishedSample=True
                        print('PY2server message '+self.strip_header(self.bit2))
                    elif 'FinishedBackground' in self.strip_header(self.bit2):
                        self.seenFinishedSample=True
                        print('PY2server message '+self.strip_header(self.bit2))
                    elif 'FinishedTransfer' in self.strip_header(self.bit2):
                        self.seenFinishedSample=True
                        print('PY2server message '+self.strip_header(self.bit2))

    def sendCommand(self,command):
        '''Sends command to the server. Returns a string with the formatted output from the server.'''
        if command!='Help':
            self.sock1.sendall(self.make_header(command).encode())
            time.sleep(self.delay) # Sleep for one second so that the thread has a delay high enough to retrieve the correct output
            if self.reading==True:
                self.feedback=self.formatMessage(self.bit2,command)
                if self.printFeedback:
                    print(self.feedback)
        else:
            if self.reading==True:
                self.feedback=self.formatMessage("...",command)
                if self.printFeedback:
                    print(self.feedback)
                    
    def enablePrintFeedback(self):
        '''
        Enables the printing of self.feedback in the python console after each sent command.
        Printing is disabled by default.
        '''
        self.printFeedback = True
        
    def disablePrintFeedback(self):
        '''
        Disables the printing of self.feedback in the python console after each sent command.
        Printing is disabled by default.
        '''
        self.printFeedback = False                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        

if __name__=="__main__":
    #   Create new connection
    newconnection=PyToServer("192.168.1.165")
    newconnection.startReading()
    #   Send command and retrieve server feedback
    newconnection.sendCommand("Limits")
    print(newconnection.feedback)
    #   Close connection
    newconnection.closeConnection()
